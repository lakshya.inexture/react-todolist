import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

class Input extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: "",
            list: [],
        };
        // Binding functions to "this"
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.removeItem = this.removeItem.bind(this);
    }
    // Set input state
    handleChange(e) {
        this.setState({
            input: e.target.value,
        });
    }
    // Add item to list and reset state
    handleSubmit(e) {
        e.preventDefault();
        const listValue = [this.state.input];
        this.setState({
            list: this.state.list.push(listValue),
        });
        this.setState({
            input: "",
            list: [...this.state.list],
        });
    }
    // Remove items by using splice method and 2nd parameter as the number of items to remove
    removeItem(item) {
        const items = this.state.list;
        items.splice(item, 1);
        this.setState({
            list: items,
        });
    }
    render() {
        return (
            <div className="container center">
                <form onSubmit={this.handleSubmit}>
                    <input
                        type="text"
                        onChange={this.handleChange}
                        value={this.state.input}
                    />
                    <button type="submit" className="btn btn-outline-dark">
                        Add Task
                    </button>
                    {this.state.list.map((value) => {
                        return (
                            <div>
                                <p>
                                    {value}
                                    <button
                                        type="button"
                                        onClick={this.removeItem}
                                        className="btn btn-outline-dark"
                                    >
                                        Completed
                                    </button>
                                </p>
                            </div>
                        );
                    })}
                </form>
            </div>
        );
    }
}

ReactDOM.render(<Input />, document.getElementById("root"));
